function resize(){
 	function equalWidget1(){
 		var h=0;
 		$('#about .bottom div.box').height('auto');
 		$('#about .bottom div.box').each(function(){
 			var height=$(this).height();
 			if(height>h){
 				h=height;
 			}
 		});
 		$('#about .bottom div.box').height(h);
 	}
 	equalWidget1();

 	function equalWidget2(){
 		var h=0;
 		$('#achievements ul li').height('auto');
 		$('#achievements ul li').each(function(){
 			var height=$(this).height();
 			if(height>h){
 				h=height;
 			}
 		});
 		$('#achievements ul li').height(h);
 	}
 	equalWidget2();

 	function equalWidget3(){
 		var h=0;
 		$('#gallery ul li').height('auto');
 		$('#gallery ul li').each(function(){
 			var height=$(this).height();
 			if(height>h){
 				h=height;
 			}
 		});
 		$('#gallery ul li').height(h);
 	}
 	equalWidget3();

 	function equalWidget4(){
 		var h=0;
 		$('#testimonials ul li').height('auto');
 		$('#testimonials ul li').each(function(){
 			var height=$(this).height();
 			if(height>h){
 				h=height;
 			}
 		});
 		$('#testimonials ul li').height(h);
 	}
 	equalWidget4();

 	function equalWidget5(){
 		var h=0;
 		$('#news ul li').height('auto');
 		$('#news ul li').each(function(){
 			var height=$(this).height();
 			if(height>h){
 				h=height;
 			}
 		});
 		$('#news ul li').height(h);
 	}
 	equalWidget5();

 	function equalWidget6(){
 		var h=0;
 		$('#news ul li div.bottom p').height('auto');
 		$('#news ul li div.bottom p').each(function(){
 			var height=$(this).height();
 			if(height>h){
 				h=height;
 			}
 		});
 		$('#news ul li div.bottom p').height(h);
 	}
 	equalWidget6();

 	function equalWidget7(){
 		var h=0;
 		$('#messages div.message').height('auto');
 		$('#messages div.message').each(function(){
 			var height=$(this).height();
 			if(height>h){
 				h=height;
 			}
 		});
 		$('#messages div.message').height(h);
 	}
 	equalWidget7();

 	function equalWidget8(){
 		var h=0;
 		$('#messages div.message p').height('auto');
 		$('#messages div.message p').each(function(){
 			var height=$(this).height();
 			if(height>h){
 				h=height;
 			}
 		});
 		$('#messages div.message p').height(h);
 	}
 	equalWidget8();

 	function equalWidget9(){
 		var h=0;
 		$('#videos ul li').height('auto');
 		$('#videos ul li').each(function(){
 			var height=$(this).height();
 			if(height>h){
 				h=height;
 			}
 		});
 		$('#videos ul li').height(h);
 	}
 	equalWidget9();
 }		
$(document).ready(function(){
	resize();
	$(".owl-carousel").owlCarousel({
	    	"loop" : true,
	    	"items" : 1,
	    	"auto" : true,
	    	"pager" : true,
	    	"autoplay" : true,
	    	"autoplaySpeed" : 1000
	    });
    
 });
 
